FROM ermaker/keras

RUN conda install -y \
    jupyter \
    matplotlib \
    seaborn

RUN pip install flask \
    tensorflow --upgrade

RUN  pip install numpy \
     scipy \
     scikit-learn

VOLUME /app
WORKDIR /app

EXPOSE 8888

COPY . /app
WORKDIR /app

ENTRYPOINT ["./start.sh"]