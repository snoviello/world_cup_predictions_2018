## Start

```
docker build -t wcup .
docker run -v $(pwd):/app -it --rm -p 8888:8888 wcup
```

## export notebook
`jupyter nbconvert --template=hidecode.tpl --to html wcup.ipynb`